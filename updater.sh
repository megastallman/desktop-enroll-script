#!/bin/bash

sudo apt update
sudo apt upgrade -y

sudo apt -y install moc vim mpv smplayer ncdu gimp ansible alien bmon checkinstall chkservice dia build-essential git handbrake bmon iftop nmon iotop ipcalc jq kdenlive keepassx kpartx krdc latencytop mencoder ncdu nmap p7zip-full powertop pwgen qgit recordmydesktop sonic-visualiser subversion tmux tree unrar vokoscreen gtkmorph docker.io docker-compose sct ngspice socat caneda pv libreoffice djview4 libnotify-bin xmlstarlet xournal inxi dstat binwalk apktool pandoc earlyoom forkstat ioping audacious nethogs virt-manager jmtpfs kio-extras-data mtp-tools git-cola kompare giggle htop freecad awscli

sudo apt purge mlocate -y
sudo apt purge firefox -y
sudo apt purge thunderbird -y
sudo apt purge unattended-upgrades -y
sudo apt purge geoip-database -y
sudo apt purge libgeoclue-2-0 -y
sudo apt purge geoclue-2.0 -y
sudo apt purge ubuntu-report -y
sudo apt purge apport -y # removes: apport-symptoms kubuntu-notification-helper libkubuntu1 python3-systemd qapt-batch
sudo apt purge whoopsie -y
sudo apt purge snapd -y

sudo systemctl disable ssh
#sudo systemctl disable docker
#sudo systemctl disable nginx

sudo rm /etc/update-motd.d/50-motd-news || true
sudo rm /etc/cron.daily/popularity-contest || true
sudo rm /etc/cron.d/popularity-contest || true
sudo rm /etc/cron.daily/ubuntu-advantage-tools || true
sudo rm /etc/cron.daily/apt-compat || true
sudo rm /etc/cron.weekly/apt-xapian-index || true
sudo rm /etc/cron.daily/slack || true
sudo rm /etc/update-motd.d/91-release-upgrade || true

sudo systemctl stop packagekit-offline-update
sudo systemctl disable packagekit-offline-update
sudo systemctl stop packagekit
sudo systemctl disable packagekit

#sudo snap refresh
#sudo snap set system refresh.metered=hold
#sudo systemctl stop snapd.service
#sudo systemctl disable snapd.service

sudo apt autoremove -y

sudo systemctl stop cups-browsed avahi-daemon
sudo systemctl disable avahi-daemon

sudo journalctl --rotate
sleep 2
sudo journalctl --vacuum-time=10s
journalctl --disk-usage

sudo fstrim -v /

sync
echo 3 | sudo tee /proc/sys/vm/drop_caches
echo 1 | sudo tee /proc/sys/vm/compact_memory

rm -rf ~/.config/kde.org
ln -sf /bin/true ~/.config/kde.org
balooctl disable


# net.ipv4.tcp_congestion_control=bbr
